# pylint: disable=unused-import
from pytest import mark

from torxtools import ctxtools


# TODO: unit-test: Verify exit code, verify stderr
@mark.parametrize(
    "exception, value, thrown",
    # fmt: off
    [
        (None, 0, None),
        (SystemExit, 0, True),
        (SystemExit, "foobar", True),
        (ValueError, 0, True),
        (ValueError, "foobar", True),
        (KeyboardInterrupt, 0, True),
        (KeyboardInterrupt, "foobar", True),
    ],
    # fmt: on
)
def test_suppress_traceback(exception, value, thrown):
    try:
        with ctxtools.suppress_traceback():
            if exception is not None:
                raise exception(value)
        assert thrown is None, "SystemExit was not called, but should have been"
    except SystemExit:
        assert thrown is not None, "SystemExit was called, but should not have been"
